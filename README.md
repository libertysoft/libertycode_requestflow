LibertyCode_RequestFlow
=======================



Description
-----------

Library contains request/response and request flow process components. 
Request flow uses front controller system, 
to provide response, from request.

---



Requirement
-----------

- Script language: PHP: version 7 || 8

---



Installation
------------

Several ways are possible:

#### Composer

1. Requirement
    
    It requires composer installation.
    For more information: https://getcomposer.org
    
2. Command: Move in project root path
    
    ```sh
    cd "<project_root_path>"
    ```

3. Command: Installation
    
    ```sh
    php composer.phar require liberty_code/request_flow ["<version>"]
    ```
    
4. Note

    - Include vendor
        
        If project uses composer, 
        vendor must be included:
        
        ```php
        require_once('<project_root_path>/vendor/autoload.php');
        ```
    
    - Configuration
    
        Installation command allows to add, 
        on composer file "<project_root_path>/composer.json",
        following configuration:
        
        ```json
        {
            "require": {
                "liberty_code/request_flow": "<version>"
            }
        }
        ```

#### Include

1. Download
    
    - Download following repository.
    - Put it on repository root path.
    
2. Include source
    
    ```php
    require_once('<repository_root_path>/include/Include.php');
    ```

---



Configuration
-------------

#### Main configuration

- Use following class to configure specific elements
    
    ```php
    use liberty_code\request_flow\config\model\DefaultConfig;
    DefaultConfig::instanceGetDefault()->get|set...();
    ```

- Elements configurables

    - Response options

---



Usage
-----

#### Request

Request allows to design the request part, 
from the request flow process.
Request allows to provide route source, 
used in request flow process, to get a response.

_Elements_

- Request

    Allows to design a basic request,  
    to get string route source.

_Example_

```php
class HttpRequestTest extends liberty_code\request_flow\request\model\DefaultRequest
{
    // Define rules to get route source
    public function getStrRouteSrc()
    {
        // Get route source from GET "route" argument
        return $_GET['route'];
    }
}

$request = HttpRequestTest::instanceGetDefault();
echo($request->getStrRouteSrc()); // Show "<route>"
...
```

#### Response

Response allows to design the response part, 
from the request flow process.
Response allows to provide final send-able content, 
from request flow process.

_Elements_

- Response

    Allows to design a basic response, 
    where send action allows to print specified content.
    Can be used to design command line response. 
    
- Response utilities
    
    Parsed content response allows 
    to get response with specific formatted content.
    Example: JSON response, XML response, etc...   

_Example_

```php
use liberty_code\request_flow\response\model\DefaultResponse;
$objResponse = new DefaultResponse();
$objResponse->setContent(...);
...
```

#### Front controller

Front controller allows to design the request flow process.
It uses router to get response, 
from specified request.

_Elements_

- FrontController

    Allows to design a basic front controller.
    Uses router and request route source as source, 
    to find route executable and get response from it.

_Example_

```php
liberty_code\request_flow\front\model\DefaultFrontController;
use liberty_code\request_flow\front\http\model\HttpFrontController;
$frontController = new DefaultFrontController();
$frontController->setRouter(...);
$frontController->setActiveRequest(...);
...
// Show response object
var_dump($frontController->execute());
...
```

---


