<?php

// Init var
$strRootPath = dirname(__FILE__) . '/..';

// Include class
include($strRootPath . '/src/config/library/ConstConfig.php');
include($strRootPath . '/src/config/exception/DefaultResponseContentInvalidFormatException.php');
include($strRootPath . '/src/config/model/DefaultConfig.php');

include($strRootPath . '/src/request/api/RequestInterface.php');
include($strRootPath . '/src/request/model/DefaultRequest.php');

include($strRootPath . '/src/response/library/ConstResponse.php');
include($strRootPath . '/src/response/library/ToolBoxResponse.php');
include($strRootPath . '/src/response/exception/ContentInvalidFormatException.php');
include($strRootPath . '/src/response/api/ResponseInterface.php');
include($strRootPath . '/src/response/model/DefaultResponse.php');

include($strRootPath . '/src/front/library/ConstFrontController.php');
include($strRootPath . '/src/front/exception/RouterInvalidFormatException.php');
include($strRootPath . '/src/front/exception/ActiveRequestInvalidFormatException.php');
include($strRootPath . '/src/front/exception/DefaultResponseInvalidFormatException.php');
include($strRootPath . '/src/front/exception/DefaultResponseUseInvalidFormatException.php');
include($strRootPath . '/src/front/exception/OptSelectResponseInvalidFormatException.php');
include($strRootPath . '/src/front/api/FrontControllerInterface.php');
include($strRootPath . '/src/front/model/DefaultFrontController.php');