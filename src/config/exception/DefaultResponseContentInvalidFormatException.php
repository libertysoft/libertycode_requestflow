<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\request_flow\config\exception;

use liberty_code\request_flow\config\library\ConstConfig;



class DefaultResponseContentInvalidFormatException extends \Exception
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Constructor / Destructor
	// ******************************************************************************
	
	/**
	 * Constructor
     * 
	 * @param mixed $content
     */
	public function __construct($content)
	{
		// Call parent constructor
		parent::__construct();
		
		// Init var
		$this->message = sprintf(ConstConfig::EXCEPT_MSG_DEFAULT_RESPONSE_CONTENT_INVALID_FORMAT, strval($content));
	}
	
	
	
	
	
	// Methods statics security (throw exception if check not pass)
	// ******************************************************************************
	
	/**
	 * Check if specified content has valid format.
	 * 
     * @param mixed $content
	 * @return boolean
	 * @throws static
     */
	static public function setCheck($content)
    {
		// Init var
		$result = 
			is_null($content) || // Check is null
			is_string($content); // Check is string
		
		// Throw exception if check not pass
		if(!$result)
		{
			throw new static($content);
		}
		
		// Return result
		return $result;
    }
	
	
	
}