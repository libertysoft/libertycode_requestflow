<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\request_flow\config\library;



class ConstConfig
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************

    // Data constants
	const DATA_KEY_DEFAULT_RESPONSE_CONTENT = 'strDefaultResponseContent';

	const DATA_DEFAULT_VALUE_DEFAULT_RESPONSE_CONTENT = null;
	
	
	
    // Exception message constants
    const EXCEPT_MSG_DEFAULT_RESPONSE_CONTENT_INVALID_FORMAT = 'Following default response content "%1$s" invalid! The content must be null or a string.';
}