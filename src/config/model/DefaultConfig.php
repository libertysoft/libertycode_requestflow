<?php
/**
 * Description :
 * This class allows to provide configuration for request flow features.
 * 
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\request_flow\config\model;

use liberty_code\library\bean\model\FixBean;

use liberty_code\request_flow\config\library\ConstConfig;
use liberty_code\request_flow\config\exception\DefaultResponseContentInvalidFormatException;



/**
 * @method null|string getStrDefaultResponseContent() Get default response content to send if response content is null.
 * @method void setStrDefaultResponseContent(string $strContent) Set default response content to send if response content is null.
 */
class DefaultConfig extends FixBean
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	/**
	 * Only 1 instance authorized (Singleton)
     * @var int
     */
	static protected $__instanceIntCountLimit = 1;
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Methods initialize
	// ******************************************************************************
	
	/**
	 * @inheritdoc
	 */
	protected function beanHydrateDefault()
	{
		if(!$this->beanExists(ConstConfig::DATA_KEY_DEFAULT_RESPONSE_CONTENT))
        {
            $this->beanAdd(ConstConfig::DATA_KEY_DEFAULT_RESPONSE_CONTENT, ConstConfig::DATA_DEFAULT_VALUE_DEFAULT_RESPONSE_CONTENT);
        }
	}





    // Methods validation
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function beanCheckValidKey($key, &$error = null)
    {
        // Init var
        $tabKey = array(
            ConstConfig::DATA_KEY_DEFAULT_RESPONSE_CONTENT
        );
        $result = in_array($key, $tabKey);

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function beanCheckValidValue($key, $value, &$error = null)
    {
        // Init var
        $result = true;

        // Validation
        try
        {
            switch($key)
            {
                case ConstConfig::DATA_KEY_DEFAULT_RESPONSE_CONTENT:
                    DefaultResponseContentInvalidFormatException::setCheck($value);
                    break;
            }
        }
        catch(\Exception $e)
        {
            $result = false;
            $error = $e;
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function beanCheckValidRemove($key, &$error = null)
    {
        // Return result
        return false;
    }
	
	
	
}