<?php
/**
 * Description :
 * This class allows to describe behavior of front controller class.
 * Front controller allows to model the request flow process part, to get response from request and router.
 * 
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\request_flow\front\api;

use liberty_code\route\route\api\RouteInterface;
use liberty_code\route\router\api\RouterInterface;
use liberty_code\request_flow\request\api\RequestInterface;
use liberty_code\request_flow\response\api\ResponseInterface;



interface FrontControllerInterface
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Methods check
	// ******************************************************************************
	
	/**
	 * Check if default response used if no response found, during execution.
	 *
	 * @return boolean
	 */
	public function checkDefaultResponseUse();
	
	
	
	
	
	// Methods getters
	// ******************************************************************************
	
	/**
	 * Get router object.
	 *
	 * @return null|RouterInterface
	 */
	public function getObjRouter();
	
	
	
	/**
	 * Get active request object.
	 * This request is used during execution.
	 *
	 * @return null|RequestInterface
	 */
	public function getObjActiveRequest();
	
	
	
	/**
	 * Get index array of active route(s).
	 * This routes are determined during execution.
	 * 
	 * @return RouteInterface[]
	 */
	public function getTabActiveRoute();



    /**
     * Get 1 specific active route from index array of active route(s) (getTabActiveRoute()).
     * Null means no specific active route is selected from array of active route(s).
     *
     * @return null|RouteInterface
     */
    public function getObjActiveRoute();


	
	/**
	 * Get default response object.
	 * This response is send if no response found during execution.
	 *
	 * @return null|ResponseInterface
	 */
	public function getObjDefaultResponse();
	
	
	
	/**
	 * Get select response rule, during execution, in case of multi match router.
	 *
	 * @return string|integer
	 */
	public function getOptSelectResponse();
	
	
	
	
	
	// Methods setters
	// ******************************************************************************
	
	/**
	 * Set router object.
	 * 
	 * @param RouterInterface $objRouter
	 */
	public function setRouter(RouterInterface $objRouter);
	
	
	
	/**
	 * Set active request object.
	 * This request is used during execution.
	 * 
	 * @param RequestInterface $objRequest
	 */
	public function setActiveRequest(RequestInterface $objRequest);
	
	
	
	/**
	 * Set default response object.
	 * This response is send if no response found during execution.
	 * 
	 * @param ResponseInterface $objResponse
	 */
	public function setDefaultResponse(ResponseInterface $objResponse);
	
	
	
	/**
	 * Set default response used option, if no response found, during execution.
	 *
	 * @param boolean $boolUse
	 */
	public function setDefaultResponseUse($boolUse);
	
	
	
	/**
	 * Set select response rule, during execution, in case of multi match router.
	 *
	 * @param string|integer $optSelectResponse
	 */
	public function setOptSelectResponse($optSelectResponse);
	
	
	
	
	
	// Methods execute
	// ******************************************************************************
	
	/**
	 * Execute active request source with router.
	 * 
	 * @return null|ResponseInterface
	 */
	public function execute();
	
	
	
	/**
	 * Execute specified route key,
     * from specified array of arguments (for call destination),
     * and specified array of string elements (for call destination customization).
	 * 
	 * @param string $strKey
	 * @param array $tabCallArg = array()
     * @param array $tabStrCallElm = array()
	 * @return null|ResponseInterface
	 */
	public function executeRoute(
	    $strKey,
        array $tabCallArg = array(),
        array $tabStrCallElm = array()
    );
}