<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\request_flow\front\exception;

use liberty_code\request_flow\front\library\ConstFrontController;



class DefaultResponseUseInvalidFormatException extends \Exception
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Constructor / Destructor
	// ******************************************************************************
	
	/**
	 * Constructor
	 * 
	 * @param mixed $use
     */
	public function __construct($use)
	{
		// Call parent constructor
		parent::__construct();
		
		// Init var
		$this->message = sprintf(ConstFrontController::EXCEPT_MSG_DEFAULT_RESPONSE_USE_INVALID_FORMAT, strval($use));
	}
	
	
	
	
	
	// Methods statics security (throw exception if check not pass)
	// ******************************************************************************
	
	/**
	 * Check if specified use option has valid format
	 * 
     * @param mixed $use
	 * @return boolean
	 * @throws static
     */
	static public function setCheck($use)
    {
		// Init var
		$result = is_bool($use);
		
		// Throw exception if check not pass
		if(!$result)
		{
			throw new static($use);
		}
		
		// Return result
		return $result;
    }
	
	
	
}