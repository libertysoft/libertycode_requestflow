<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\request_flow\front\exception;

use liberty_code\request_flow\front\library\ConstFrontController;



class OptSelectResponseInvalidFormatException extends \Exception
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Constructor / Destructor
	// ******************************************************************************
	
	/**
	 * Constructor
     * 
	 * @param mixed $option
     */
	public function __construct($option)
	{
		// Call parent constructor
		parent::__construct();
		
		// Init var
		$this->message = sprintf(ConstFrontController::EXCEPT_MSG_OPT_SELECT_RESPONSE_INVALID_FORMAT, strval($option));
	}
	
	
	
	
	
	// Methods statics security (throw exception if check not pass)
	// ******************************************************************************
	
	/**
	 * Check if specified option has valid format
	 * 
     * @param mixed $option
	 * @return boolean
	 * @throws static
     */
	static public function setCheck($option)
    {
		// Init var
		$tabOption = ConstFrontController::getTabSelectResponseOption();
		$result = 
			(is_integer($option) && ($option >= 0)) || // Check is valid integer (array index)
			(is_string($option) && in_array($option, $tabOption)); // Check is valid option
		
		// Throw exception if check not pass
		if(!$result)
		{
			throw new static($option);
		}
		
		// Return result
		return $result;
    }
	
	
	
}