<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\request_flow\front\exception;

use liberty_code\route\router\api\RouterInterface;
use liberty_code\request_flow\front\library\ConstFrontController;



class RouterInvalidFormatException extends \Exception
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Constructor / Destructor
	// ******************************************************************************
	
	/**
	 * Constructor
	 * 
	 * @param mixed $router
     */
	public function __construct($router)
	{
		// Call parent constructor
		parent::__construct();
		
		// Init var
		$this->message = sprintf
        (
            ConstFrontController::EXCEPT_MSG_ROUTER_INVALID_FORMAT,
            mb_strimwidth(strval($router), 0, 10, "...")
        );
	}
	
	
	
	
	
	// Methods statics security (throw exception if check not pass)
	// ******************************************************************************
	
	/**
	 * Check if specified router has valid format
	 * 
     * @param mixed $router
	 * @return boolean
	 * @throws static
     */
	static public function setCheck($router)
    {
		// Init var
		$result = (
			(is_null($router)) ||
			($router instanceof RouterInterface)
		);
		
		// Throw exception if check not pass
		if(!$result)
		{
			throw new static($router);
		}
		
		// Return result
		return $result;
    }
	
	
	
}