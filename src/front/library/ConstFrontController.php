<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\request_flow\front\library;



class ConstFrontController
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************
	
	// Data constants
    const DATA_KEY_ROUTER = 'objRouter';
	const DATA_KEY_ACTIVE_REQUEST = 'objActiveRequest';
	const DATA_KEY_DEFAULT_RESPONSE = 'objDefaultResponse';
	const DATA_KEY_DEFAULT_RESPONSE_USE = 'boolDefaultResponseUse';
	const DATA_KEY_OPT_SELECT_RESPONSE = 'optSelectReponse';
	
	
	
	// Configuration
	const OPTION_SELECT_RESPONSE_VALUE_FIRST = 'select_response_first';
	const OPTION_SELECT_RESPONSE_VALUE_LAST = 'select_response_last';
	
	
	
	// Exception message constants
    const EXCEPT_MSG_ROUTER_INVALID_FORMAT = 'Following router "%1$s" invalid! It must be a router object.';
	const EXCEPT_MSG_ACTIVE_REQUEST_INVALID_FORMAT = 'Following active request "%1$s" invalid! It must be a request object.';
	const EXCEPT_MSG_DEFAULT_RESPONSE_INVALID_FORMAT = 'Following default response "%1$s" invalid! It must be a response object.';
	const EXCEPT_MSG_DEFAULT_RESPONSE_USE_INVALID_FORMAT = 'Following default response use option "%1$s" invalid! The option must be a boolean.';
	const EXCEPT_MSG_OPT_SELECT_RESPONSE_INVALID_FORMAT = 'Following select response option "%1$s" invalid! The option must be a valid option.';
	
	
	
	
	
	// ******************************************************************************
    // Methods
    // ******************************************************************************

    // Methods statics getters
    // ******************************************************************************

    /**
     * Get select response options array
     *
     * @return array
     */
    static public function getTabSelectResponseOption()
    {
        // Init var
        $result = array(
            self::OPTION_SELECT_RESPONSE_VALUE_FIRST,
            self::OPTION_SELECT_RESPONSE_VALUE_LAST
        );

        // Return result
        return $result;
    }
	
	
	
}