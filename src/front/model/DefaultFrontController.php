<?php
/**
 * Description :
 * This class allows to define default front controller class.
 * Can be consider is base of all front controllers type.
 * 
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\request_flow\front\model;

use liberty_code\library\bean\model\FixBean;
use liberty_code\request_flow\front\api\FrontControllerInterface;

use liberty_code\route\route\api\RouteInterface;
use liberty_code\route\router\api\RouterInterface;
use liberty_code\request_flow\config\model\DefaultConfig;
use liberty_code\request_flow\request\api\RequestInterface;
use liberty_code\request_flow\response\api\ResponseInterface;
use liberty_code\request_flow\front\library\ConstFrontController;
use liberty_code\request_flow\front\exception\RouterInvalidFormatException;
use liberty_code\request_flow\front\exception\ActiveRequestInvalidFormatException;
use liberty_code\request_flow\front\exception\DefaultResponseInvalidFormatException;
use liberty_code\request_flow\front\exception\DefaultResponseUseInvalidFormatException;
use liberty_code\request_flow\front\exception\OptSelectResponseInvalidFormatException;



class DefaultFrontController extends FixBean implements FrontControllerInterface
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	
	
	/** @var RouteInterface[] */
    protected $tabActiveRoute;
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc
     * @param RouterInterface $objRouter = null
     * @param RequestInterface $objActiveRequest = null
	 * @param ResponseInterface $objDefaultResponse = null
	 * @param boolean $boolDefaultResponseUse = null
	 * @param string|integer $optSelectResponse = null
     */
    public function __construct(RouterInterface $objRouter = null, RequestInterface $objActiveRequest = null, ResponseInterface $objDefaultResponse = null, $boolDefaultResponseUse = null, $optSelectResponse = null)
    {
        // Call parent constructor
        parent::__construct();

        // Init router if required
        if(!is_null($objRouter))
        {
            $this->setRouter($objRouter);
        }
		
		// Init active request if required
        if(!is_null($objActiveRequest))
        {
            $this->setActiveRequest($objActiveRequest);
        }
		
		// Init default response if required
        if(!is_null($objDefaultResponse))
        {
            $this->setDefaultResponse($objDefaultResponse);
        }
		
		// Init option default response use
        if(!is_null($boolDefaultResponseUse))
        {
            $this->setDefaultResponseUse($boolDefaultResponseUse);
        }
		
		// Init option select response
        if(!is_null($optSelectResponse))
        {
            $this->setOptSelectResponse($optSelectResponse);
        }
    }
	
	
	
	
	
    // Methods initialize
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function beanHydrateDefault()
    {
		// Init var
        $this->initTabActiveRoute();
		
        // Init bean data
		if(!$this->beanExists(ConstFrontController::DATA_KEY_ROUTER))
        {
            $this->beanAdd(ConstFrontController::DATA_KEY_ROUTER, null);
        }
		
		if(!$this->beanExists(ConstFrontController::DATA_KEY_ACTIVE_REQUEST))
        {
            $this->beanAdd(ConstFrontController::DATA_KEY_ACTIVE_REQUEST, null);
        }
		
		if(!$this->beanExists(ConstFrontController::DATA_KEY_DEFAULT_RESPONSE))
        {
            $this->beanAdd(ConstFrontController::DATA_KEY_DEFAULT_RESPONSE, null);
        }
		
		if(!$this->beanExists(ConstFrontController::DATA_KEY_DEFAULT_RESPONSE_USE))
        {
            $this->beanAdd(ConstFrontController::DATA_KEY_DEFAULT_RESPONSE_USE, true);
        }
		
		if(!$this->beanExists(ConstFrontController::DATA_KEY_OPT_SELECT_RESPONSE))
        {
            $this->beanAdd(ConstFrontController::DATA_KEY_OPT_SELECT_RESPONSE, ConstFrontController::OPTION_SELECT_RESPONSE_VALUE_LAST);
        }
    }
	
	
	
	/**
     * Initialize index array of active routes.
     */
    protected function initTabActiveRoute()
    {
        // Init var
        $this->tabActiveRoute = array();
    }





	// Methods validation
	// ******************************************************************************

	/**
	 * @inheritdoc
	 */
	public function beanCheckValidKey($key, &$error = null)
	{
		// Init var
		$tabKey = array(
			ConstFrontController::DATA_KEY_ROUTER,
			ConstFrontController::DATA_KEY_ACTIVE_REQUEST,
			ConstFrontController::DATA_KEY_DEFAULT_RESPONSE,
			ConstFrontController::DATA_KEY_DEFAULT_RESPONSE_USE,
			ConstFrontController::DATA_KEY_OPT_SELECT_RESPONSE
		);
		$result = in_array($key, $tabKey);

		// Return result
		return $result;
	}



	/**
	 * @inheritdoc
	 */
	public function beanCheckValidValue($key, $value, &$error = null)
	{
		// Init var
		$result = true;

		// Validation
		try
		{
			switch($key)
			{
				case ConstFrontController::DATA_KEY_ROUTER:
					RouterInvalidFormatException::setCheck($value);
					break;

				case ConstFrontController::DATA_KEY_ACTIVE_REQUEST:
					ActiveRequestInvalidFormatException::setCheck($value);
					break;

				case ConstFrontController::DATA_KEY_DEFAULT_RESPONSE:
					DefaultResponseInvalidFormatException::setCheck($value);
					break;

				case ConstFrontController::DATA_KEY_DEFAULT_RESPONSE_USE:
					DefaultResponseUseInvalidFormatException::setCheck($value);
					break;

				case ConstFrontController::DATA_KEY_OPT_SELECT_RESPONSE:
					OptSelectResponseInvalidFormatException::setCheck($value);
					break;
			}
		}
		catch(\Exception $e)
		{
			$result = false;
			$error = $e;
		}

		// Return result
		return $result;
	}



	/**
	 * @inheritdoc
	 */
	public function beanCheckValidRemove($key, &$error = null)
	{
		// Return result
		return false;
	}




	
	// Methods check
	// ******************************************************************************
	
	/**
	 * @inheritdoc
	 */
	public function checkDefaultResponseUse()
	{
		// Return result
        return $this->beanGet(ConstFrontController::DATA_KEY_DEFAULT_RESPONSE_USE);
	}
	
	
	
	
	
    // Methods getters
	// ******************************************************************************

    /**
     * Get item from request routing multi match execution.
     *
     * @param mixed $tabItem
     * @return null|mixed
     */
    protected function getItemFromMultiExec(array $tabItem)
    {
        // Init var
        $result = null;
        $optSelectResponse = $this->getOptSelectResponse();

        // Check at list one item found
        if(is_array($tabItem) && (count($tabItem) > 0))
        {
            // Select item
            $item = null;

            // Get first item if required
            if(
                ($optSelectResponse == ConstFrontController::OPTION_SELECT_RESPONSE_VALUE_FIRST) ||
                (is_integer($optSelectResponse) && ($optSelectResponse < 0))
            )
            {
                $item = $tabItem[0];
            }
            // Get last item if required
            else if(
                ($optSelectResponse == ConstFrontController::OPTION_SELECT_RESPONSE_VALUE_LAST) ||
                (is_integer($optSelectResponse) && ($optSelectResponse >= count($tabItem)))
            )
            {
                $item = $tabItem[(count($tabItem) - 1)];
            }
            // Get specified item if required
            else if(
                is_integer($optSelectResponse) &&
                ($optSelectResponse >= 0) && ($optSelectResponse < count($tabItem))
            )
            {
                $item = $tabItem[$optSelectResponse];
            }

            // Register item
            $result = $item;
        }

        // Return result
        return $result;
    }



	/**
	 * @inheritdoc
	 */
	public function getObjRouter()
	{
		// Return result
        return $this->beanGet(ConstFrontController::DATA_KEY_ROUTER);
	}
	
	
	
	/**
	 * @inheritdoc
	 */
	public function getObjActiveRequest()
	{
		// Return result
        return $this->beanGet(ConstFrontController::DATA_KEY_ACTIVE_REQUEST);
	}
	
	
	
	/**
	 * @inheritdoc
	 */
	public function getTabActiveRoute()
	{
		// Return result
        return $this->tabActiveRoute;
	}



    /**
     * @inheritdoc
     * Get 1 active route from active routes, based on request routing multi match execution.
     */
    public function getObjActiveRoute()
    {
        // Return result
        return $this->getItemFromMultiExec($this->tabActiveRoute);
    }
	
	
	
	/**
	 * @inheritdoc
	 */
	public function getObjDefaultResponse()
	{
		// Return result
        return $this->beanGet(ConstFrontController::DATA_KEY_DEFAULT_RESPONSE);
	}
	
	
	
	/**
	 * @inheritdoc
	 */
	public function getOptSelectResponse()
	{
		// Return result
        return $this->beanGet(ConstFrontController::DATA_KEY_OPT_SELECT_RESPONSE);
	}
	
	
	
	
	
    // Methods setters
	// ******************************************************************************
	
	/**
	 * @inheritdoc
	 */
	public function setRouter(RouterInterface $objRouter)
	{
		$this->beanSet(ConstFrontController::DATA_KEY_ROUTER, $objRouter);
	}
	
	
	
	/**
	 * @inheritdoc
	 */
	public function setActiveRequest(RequestInterface $objRequest)
	{
		$this->beanSet(ConstFrontController::DATA_KEY_ACTIVE_REQUEST, $objRequest);
		
		// Init active route(s)
        $this->initTabActiveRoute();
	}
	
	
	
	/**
	 * @inheritdoc
	 */
	public function setDefaultResponse(ResponseInterface $objResponse)
	{
		$this->beanSet(ConstFrontController::DATA_KEY_DEFAULT_RESPONSE, $objResponse);
	}
	
	
	
	/**
	 * @inheritdoc
	 */
	public function setDefaultResponseUse($boolUse)
	{
		$this->beanSet(ConstFrontController::DATA_KEY_DEFAULT_RESPONSE_USE, $boolUse);
	}
	
	
	
	/**
	 * @inheritdoc
	 */
	public function setOptSelectResponse($optSelectResponse)
	{
		$this->beanSet(ConstFrontController::DATA_KEY_OPT_SELECT_RESPONSE, $optSelectResponse);
	}

	
	
	

	// Methods execute
	// ******************************************************************************

	/**
	 * Get object response from request routing multi match execution.
	 * 
	 * @param mixed $tabResponse
	 * @return null|ResponseInterface
	 */
	protected function getObjResponseFromMultiExec($tabResponse)
	{
		// Return result
        return $this->getObjResponseFromExec($this->getItemFromMultiExec($tabResponse));
	}
	
	
	
	/**
	 * Get object response from request routing execution.
	 * 
	 * @param mixed $objResponse
	 * @return null|ResponseInterface
	 */
	protected function getObjResponseFromExec($objResponse)
	{
		// Init var
		$result = null;
		$objDefaultResponse = $this->getObjDefaultResponse();
		$boolDefaultResponse = $this->checkDefaultResponseUse();
		
		// Get specified response if valid
		if($objResponse instanceof ResponseInterface)
		{
			$result = $objResponse;
		}
		// Get default response if required and valid
		else if($boolDefaultResponse && (!is_null($objDefaultResponse)))
		{
			$result = $objDefaultResponse;
		}
		
		// Return result
        return $result;
	}
	
	
	
	/**
	 * @inheritdoc
	 */
	public function execute()
	{
		// Init var
		$result = null;
		$objRouter = $this->getObjRouter();
		$objRequest = $this->getObjActiveRequest();
		$this->initTabActiveRoute();
		
		// Check router and request found
		if((!is_null($objRouter)) && (!is_null($objRequest)))
		{
			// Init var
			$strSrc = $objRequest->getStrRouteSrc();
			$objRouteCollection = $objRouter->getObjRouteCollection();
			
			// Check route collection found
			if(!is_null($objRouteCollection))
			{
				// Execution
				$this->tabActiveRoute = $objRouteCollection->getTabRoute($strSrc);
				$objResponse = $objRouter->execute($strSrc);

				// Get result
				$result = $this->getObjResponseFromMultiExec($objResponse);
			}
		}
		
		// Return result
        return $result;
	}
	
	
	
	/**
	 * @inheritdoc
	 */
	public function executeRoute(
	    $strKey,
        array $tabCallArg = array(),
        array $tabStrCallElm = array()
    )
	{
		// Init var
		$result = null;
		$objRouter = $this->getObjRouter();
		
		// Check router found
		if(!is_null($objRouter))
		{
			// Execution
			$objResponse = $objRouter->executeRoute($strKey, $tabCallArg, $tabStrCallElm);
			
			// Get result
			$result = $this->getObjResponseFromExec($objResponse);
		}
		
		// Return result
        return $result;
	}
	
	
	
	

    // Methods statics
    // ******************************************************************************
	
    /**
     * Get route configuration.
     *
     * @return DefaultConfig
     */
    public static function getObjConfig()
    {
        // Return result
        return DefaultConfig::instanceGetDefault();
    }
	
	
	
}