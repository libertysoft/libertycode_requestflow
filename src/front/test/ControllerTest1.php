<?php

namespace liberty_code\request_flow\front\test;

use liberty_code\request_flow\response\library\ToolBoxResponse;
use liberty_code\request_flow\front\model\DefaultFrontController;



class ControllerTest1
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************

	// Methods action
    // ******************************************************************************
	
    public function action($strAdd = '', $strAdd2 = '')
    {
		// Init var
		/** @var DefaultFrontController $objFrontController */
		$objFrontController = DefaultFrontController::instanceGet(0);
		
		// Init active route(s)
		$tabStrRoute = array();
		$tabRoute = $objFrontController->getTabActiveRoute();
		foreach($tabRoute as $objRoute)
		{
			$tabStrRoute[] = $objRoute->getStrKey();
		}
		
		// Get data
		$tabData = array(
			'argument' => [
				'add_1' => $strAdd,
				'add_2' => $strAdd2,
			],
			'route' => $tabStrRoute
		);
		
		// Get response
		$objResponse = ToolBoxResponse::getObjJsonResponse($tabData);
		
        // Return result
        return $objResponse;
    }
	
	
	
	public function action2($strAdd = '')
    {
		// Init var
		/** @var DefaultFrontController $objFrontController */
		// $objFrontController = DefaultFrontController::instanceGet(0);
    }
	
	
	
}