<?php

namespace liberty_code\request_flow\front\test;

use liberty_code\request_flow\response\library\ToolBoxResponse;
use liberty_code\request_flow\front\model\DefaultFrontController;



class ControllerTest2
{
    // ******************************************************************************
    // Methods
    // ******************************************************************************
	
    // Methods action
    // ******************************************************************************

    public function action($strAdd = '')
    {
        // Init var
		/** @var DefaultFrontController $objFrontController */
		$objFrontController = DefaultFrontController::instanceGet(0);
		
		// Init active route(s)
		$tabStrRoute = array();
		$tabRoute = $objFrontController->getTabActiveRoute();
		foreach($tabRoute as $objRoute)
		{
			$tabStrRoute[$objRoute->getStrKey()] = get_class($objRoute);
		}
        $objRoute = $objFrontController->getObjActiveRoute();

		// Get data
		$tabData = array(
			'root' => [
                'option_select' => strval($objFrontController->getOptSelectResponse()),
				'argument' => [
					'add' => $strAdd,
				],
				'route' => $tabStrRoute,
                'route_active' => (is_null($objRoute) ? 'null' : $objRoute->getStrKey())
			]
		);
		
		// Get response
		$objResponse = ToolBoxResponse::getObjXmlResponse($tabData);
		
        // Return result
        return $objResponse;
    }
	
	
	
	public function action2($strAdd = '')
    {
        // Init var
		/** @var DefaultFrontController $objFrontController */
		$objFrontController = DefaultFrontController::instanceGet(0);
		
		// Get forwarded response
		$objResponse = $objFrontController->executeRoute('route_2', array('strAdd' => $strAdd . '-forward'));
		
        // Return result
        return $objResponse;
    }
	
	
	
}