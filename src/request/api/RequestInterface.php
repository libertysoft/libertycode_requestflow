<?php
/**
 * Description :
 * This class allows to describe behavior of request class.
 * Request allows to model the request part from the request flow process.
 * 
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\request_flow\request\api;



interface RequestInterface
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Methods getters
	// ******************************************************************************
	
	/**
	 * Get string route source.
	 * The route source allows to represent the request in a string, usable as source for route matching.
	 * 
	 * @return string
	 */
	public function getStrRouteSrc();
}