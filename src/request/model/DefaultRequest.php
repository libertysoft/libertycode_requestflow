<?php
/**
 * Description :
 * This class allows to define default request class.
 * Can be consider is base of all request type.
 * 
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\request_flow\request\model;

use liberty_code\library\instance\model\Multiton;
use liberty_code\request_flow\request\api\RequestInterface;

use liberty_code\request_flow\config\model\DefaultConfig;



abstract class DefaultRequest extends Multiton implements RequestInterface
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods statics
    // ******************************************************************************

    /**
     * Get request flow configuration.
     *
     * @return DefaultConfig
     */
    public static function getObjConfig()
    {
        // Return result
        return DefaultConfig::instanceGetDefault();
    }



}