<?php
/**
 * Description :
 * This class allows to define HTTP request test class.
 * 
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\request_flow\request\test;

use liberty_code\request_flow\request\model\DefaultRequest;



class HttpRequestTest extends DefaultRequest
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	// static protected $__instanceTab = array();
	
	/**
	 * Only 1 instance authorized (Singleton)
     * @var int
     */
	// static protected $__instanceIntCountLimit = 1;
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

	// Methods getters
    // ******************************************************************************
	
	/**
	 * @inheritdoc
	 */
	public function getStrRouteSrc()
	{
	    $result = '';

	    // Get route, if found
	    if(array_key_exists('route', $_GET))
        {
            $result = $_GET['route'];
        }

		// Return result
		return $result;
	}
	
	
	
}