<?php
/**
 * Description :
 * This class allows to describe behavior of response class.
 * Response allows to model the response part from the request flow process.
 * 
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\request_flow\response\api;



interface ResponseInterface
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Methods execute
	// ******************************************************************************
	
	/**
	 * Send the response.
	 */
	public function send();
}