<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\request_flow\response\library;



class ConstResponse
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************

    // Data constants
    const DATA_KEY_DEFAULT_CONTENT = 'content';

    // Exception message constants
    const EXCEPT_MSG_CONTENT_INVALID_FORMAT = 'Following content "%1$s" invalid! The content must be null or a string.';
}