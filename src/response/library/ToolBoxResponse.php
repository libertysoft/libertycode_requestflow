<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\request_flow\response\library;

use liberty_code\library\instance\model\Multiton;

use liberty_code\parser\parser\api\ParserInterface;
use liberty_code\parser\parser\string_table\json\model\JsonParser;
use liberty_code\parser\parser\string_table\yml\model\JsonYmlParser;
use liberty_code\parser\parser\string_table\xml\model\DefaultXmlParser;
use liberty_code\parser\parser\string_table\xml\model\AttributeXmlParser;
use liberty_code\request_flow\response\model\DefaultResponse;



class ToolBoxResponse extends Multiton
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	/**
	 * Only 1 instance authorized (Singleton)
     * @var int
     */
	static protected $__instanceIntCountLimit = 1;
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Methods getters
	// ******************************************************************************
	
	/**
     * Get JSON response.
     *
	 * @param array $tabData
	 * @param DefaultResponse $objResponse = null
     * @return DefaultResponse
     */
    public static function getObjJsonResponse(array $tabData, DefaultResponse $objResponse = null)
    {
		// Init var
		$result = $objResponse;
		if(is_null($result))
		{
			$result = new DefaultResponse();
		}
		
		// Set content
		static::setParseContent($result, $tabData, new JsonParser());
		
		// Return result
		return $result;
    }



    /**
     * Get YML response.
     *
     * @param array $tabData
     * @param DefaultResponse $objResponse = null
     * @return DefaultResponse
     */
    public static function getObjYmlResponse(array $tabData, DefaultResponse $objResponse = null)
    {
        // Init var
        $result = $objResponse;
        if(is_null($result))
        {
            $result = new DefaultResponse();
        }

        // Set content
        static::setParseContent($result, $tabData, new JsonYmlParser());

        // Return result
        return $result;
    }



	/**
     * Get XML response.
     *
	 * @param array $tabData
	 * @param DefaultResponse $objResponse = null
     * @return DefaultResponse
     */
    public static function getObjXmlResponse(array $tabData, DefaultResponse $objResponse = null)
    {
		// Init var
		$result = $objResponse;
		if(is_null($result))
		{
			$result = new DefaultResponse();
		}
		
		// Set content
		static::setParseContent($result, $tabData, new DefaultXmlParser());
		
		// Return result
		return $result;
    }
	
	
	
	/**
     * Get XML response (with attributes).
     *
	 * @param array $tabData
	 * @param DefaultResponse $objResponse = null
     * @return DefaultResponse
     */
    public static function getObjXmlAttributeResponse(array $tabData, DefaultResponse $objResponse = null)
    {
		// Init var
		$result = $objResponse;
		if(is_null($result))
		{
			$result = new DefaultResponse();
		}
		
		// Set content
		static::setParseContent($result, $tabData, new AttributeXmlParser());
		
		// Return result
		return $result;
    }
	
	
	
	
	
	// Methods setters
	// ******************************************************************************
	
	/**
     * Set specified parse content in specified response.
     *
     * @param DefaultResponse $objResponse
	 * @param mixed $data
     * @param ParserInterface $objParser
     */
    public static function setParseContent(DefaultResponse $objResponse, $data, ParserInterface $objParser)
    {
		// Init var
		$strContent = $objParser->getSource($data);
		
		// Set response content
		$objResponse->setContent($strContent);
    }
	
	
	
}