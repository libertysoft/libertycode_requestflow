<?php
/**
 * Description :
 * This class allows to define default response class.
 * Can be consider is base of all response type.
 * 
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\request_flow\response\model;

use liberty_code\library\bean\model\FixBean;
use liberty_code\request_flow\response\api\ResponseInterface;

use liberty_code\request_flow\config\model\DefaultConfig;
use liberty_code\request_flow\response\library\ConstResponse;
use liberty_code\request_flow\response\exception\ContentInvalidFormatException;



/**
 * @method null|string getContent() Get response content.
 * @method void setContent(string $strContent) Set response content.
 */
class DefaultResponse extends FixBean implements ResponseInterface
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods initialize
    // ******************************************************************************

    /**
     * Overwrite hydrateDefault().
     */
    protected function beanHydrateDefault()
    {
        // Init bean data
		if(!$this->beanExists(ConstResponse::DATA_KEY_DEFAULT_CONTENT))
        {
            $this->beanAdd(ConstResponse::DATA_KEY_DEFAULT_CONTENT, null);
        }
    }





    // Methods validation
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function beanCheckValidKey($key, &$error = null)
    {
        // Init var
        $tabKey = array(
            ConstResponse::DATA_KEY_DEFAULT_CONTENT
        );
        $result = in_array($key, $tabKey);

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function beanCheckValidValue($key, $value, &$error = null)
    {
        // Init var
        $result = true;

        // Validation
        try
        {
            switch($key)
            {
                case ConstResponse::DATA_KEY_DEFAULT_CONTENT:
                    ContentInvalidFormatException::setCheck($value);
                    break;
            }
        }
        catch(\Exception $e)
        {
            $result = false;
            $error = $e;
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function beanCheckValidRemove($key, &$error = null)
    {
        // Return result
        return false;
    }


	
	
	
	// Methods getters
    // ******************************************************************************
	
    /**
     * Get response content in string format (ready to send).
     * 
	 * @return string
     */
    protected function getStrContentToSend()
    {
		// Init var
        $result = '';
		$strDefaultContent = static::getObjConfig()->getStrDefaultResponseContent();
		$strContent = $this->getContent();
		
		// Register content in result if found
		if((!is_null($strContent)) && is_string($strContent))
		{
			$result = $strContent;
		}
		// Register default content in result if found
		else if((!is_null($strDefaultContent)) && is_string($strDefaultContent))
		{
			$result = $strDefaultContent;
		}
		
		// Return result
		return $result;
    }



	

	// Methods execute
	// ******************************************************************************
	
	/**
	 * @inheritdoc
	 */
	public function send()
	{
		// Set content
		echo($this->getStrContentToSend());
	}
	
	
	
	

    // Methods statics
    // ******************************************************************************

    /**
     * Get route configuration.
     *
     * @return DefaultConfig
     */
    public static function getObjConfig()
    {
        // Return result
        return DefaultConfig::instanceGetDefault();
    }



}