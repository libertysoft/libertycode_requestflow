<?php

// Init var
$strRootAppPath = dirname(__FILE__) . '/../../..';

// Load external library
require_once($strRootAppPath . '/vendor/autoload.php'); // Composer vendor

// Load library
require_once($strRootAppPath . '/include/Include.php');

// Use
use liberty_code\request_flow\config\model\DefaultConfig;
use liberty_code\request_flow\response\model\DefaultResponse;



// Init var
ob_start();
/** @var DefaultConfig $objConfig */
$objConfig = DefaultConfig::instanceGetDefault();
$objResponse = new DefaultResponse();



// Test configuration
echo('Test configuration: <br />');
$objConfig->setStrDefaultResponseContent('Default content');
echo('Get default content: <pre>');var_dump($objResponse::getObjConfig()->getStrDefaultResponseContent());echo('</pre>');
echo('<br /><br /><br />');



// Test properties
echo('Test properties : <br />');

try{
	$objResponse->setContent(true);
} catch(\Exception $e) {
	echo($e->getMessage());
	echo('<br />');
}

$objResponse->setContent('Response content');
echo('Get content: <pre>');print_r($objResponse->getContent());echo('</pre>');

echo('<br /><br /><br />');



// Test send
$strContent = ob_get_clean();
//$objResponse->setContent($strContent);
$objResponse->send();


